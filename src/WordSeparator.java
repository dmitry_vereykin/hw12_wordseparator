/**
 * Created by Dmitry on 6/15/2015.
 */
import java.util.Scanner;

public class WordSeparator {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        System.out.println("Enter a sentence with no spaces "
                + "between the words, and each word starts with a capital letter");
        System.out.println("For example: StopAndSmellTheRoses");
        System.out.println("----------------------------------------------------");
        System.out.print(">>> ");

        String userInput = keyboard.nextLine();

        keyboard.close();

        System.out.println("-----------------------------------------------------");
        String[] array = userInput.split("(?=[A-Z])");

        System.out.print(array[0]);
        for (int i = 1; i < array.length; i++){
            System.out.print(" " + array[i].toLowerCase());
        }
    }
}
